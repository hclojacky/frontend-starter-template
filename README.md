# Front End Workflow - Gulp Pug SASS and Babel
This is a simple workflow setup for building webpages.
It uses [Gulp.js](https://gulpjs.com/), [Pugjs](https://pugjs.org/api/getting-started.html), [SASS](https://sass-lang.com/) and [Babel](https://babeljs.io/).
## Requirement
Node.js need to be install before running Gulp.<br/>
[Down from here](https://nodejs.org/en/download/)

This workflow require Node version `10.13.0`, check the version with this command:
```
node -v
```
## Install
First it needs to be install several packages:
```
npm install
```
## Project Structure
```
gulp-pug-sass-babel
└───src
|   └───assets
|       // put all your assets in here, eg: images, videos, fonts etc.
|   └───config
|   |   // all web app config file, like web manifest, service workers
|   └───scripts
|   |   └───lib
|   |   |   lib.js // include all vendor js files from node_modules folder
|   |   base.se6 // some base scripts write in here
|   |   ... (or more)
|   └───styles
|   |   └───base
|   |   |   global.scss // write all global style
|   |   |   mixin.scss // scss mixin to help you write less code
|   |   |   utilities.scss // some common class to directly use
|   |   |   variables.scss // variables use through the whole sites
|   |   |   ... (or more)
|   |   └───lib
|   |   |   // include all vendor css files in here
|   |   |   // may be need to change to .scss extension
|   |   |   lib.scss
|   |   main.scss // import all others scss files in here, you can use '**/*.scss' to include all files from subfolder
│   └───templates
|   |   └───components
|   |   |   // components should be a mixin which could use in all template files, like button, link etc
│   |   └───includes
|   |   |   // style, meta and script separated in here
│   |   └───layout
|   |   |   // layout for different pages
|   |   └───modules
|   |   |   // modules should be a part of the pages like header and footer etc.
│   |   index.pug // all your pages in here
|   .babelrc
|   .eslintignore
|   .eslintrc
|   .gitignore
|   gulpfile.babel.js
|   package.json
|   README.md
```
## Development
### Work in "src"
To start develop, start the Gulp:
```
gulp dev
```
If you do not have Gulp install in your machine, you can run with npm:
```
npm run dev
```
It will open new tab from your default browser with local server and hard reload.
### Created "dev"
You will see a "dev" folder created, which is included compiled css, js and html files.

---

## Build
After finished your development, run this command to pack all files:
```
gulp build
```
or
```
npm run build
```
It will create a "dist" folder and include all assets and minified html files.
## Lint
To run eslint to check any javascript issue, run this command
```
gulp lint
```
or
```
npm run lint
```