// import gulp into here
import gulp from 'gulp';
// import gulp-load-plugins to help us load different gulp plugins therefore we don't need to each one by one
const $ = require('gulp-load-plugins')({
  rename: {
    'gulp-sass-glob': 'sassGlob',
    'gulp-strip-debug': 'stripDebug',
    'gulp-clean-css': 'cleancss',
  }
});
// import browser-sync for browser hard reload and sync
const browserSync = require('browser-sync').create();
// reload the browser function
const browserReload = (done) => {
  browserSync.reload();
  done();
}
// to minify html or not when building the final "dist"
const minHtml = false;

// plugin lists that can be imported, check for package.json if there is some missing
// autoprefixer
// babel
// changed
// clean
// cleancss
// concat
// eslint
// flatten
// htmlmin
// if
// imagemin
// include
// plumber
// pug
// sass
// sassGlob
// sourcemaps
// stripDebug
// uglify

// Compile pug template to html
const compilePug = () => {
  return gulp.src(['./src/templates/**/*.pug', '!./src/templates/**/_*.pug','!./src/templates/modules/**/*.pug', '!./src/templates/components/**/*.pug'])
    .pipe($.plumber({
      errorHandler(err) {
        console.log(err.message)
        this.emit('end')
      }
    }))
    .pipe($.pug({
      pretty: true,
      cache: true
    }))
    .pipe(gulp.dest('./dev/'))
}

// Watch pug template
const watchPug = () => {
  gulp.watch(['./src/templates/**/*.pug'], gulp.series(compilePug, browserReload))
}

// Compile SASS to CSS
const compileSass = () => {
  return gulp.src('./src/styles/main.scss')
    .pipe($.plumber({
      errorHandler(err) {
        console.log(err.message)
        this.emit('end')
      }
    }))
    .pipe($.sourcemaps.init())
    .pipe($.sassGlob())
    .pipe($.sass())
    .pipe($.autoprefixer())
    .pipe($.cleancss({
      level: 1,
      format: 'beautify'
    }))
    .pipe($.rename('main.css'))
    .pipe($.sourcemaps.write('./maps'))
    .pipe(gulp.dest('./dev/css'))
}

// Watch SASS files
const watchSass = () => {
  gulp.watch(['./src/styles/**/*.scss', './src/templates/**/*.scss'], gulp.series(compileSass, browserReload))
}

// Compile es6 to javascript
const compileJs = () => {
  return gulp.src([
    './src/scripts/**/*.es6',
    './src/templates/**/*.es6'
  ])
    .pipe($.plumber({
      errorHandler(err) {
        console.log(err.message)
        this.emit('end')
      }
    }))
    .pipe($.sourcemaps.init())
    .pipe($.babel())
    .pipe($.flatten())
    .pipe($.concat('main.js'))
    .pipe($.sourcemaps.write('./maps'))
    .pipe(gulp.dest('./dev/js'))
}

// Wathc es6 files
const watchJs = () => {
  gulp.watch(['./src/scripts/**/*.es6', './src/templates/**/*.es6'], gulp.series(compileJs, browserReload))
}

// Compile js library and copy to dev
const compileJsLib = () => {
  return gulp.src('./src/scripts/lib/lib.js')
    .pipe($.plumber({
      errorHandler(err) {
        console.log(err.message)
        this.emit('end')
      }
    }))
    .pipe($.include({
      includePaths: [
        __dirname + "/node_modules"
      ]
    })).on('error', console.log)
    .pipe(gulp.dest("./dev/js"))
}

// Watch js library
const watchJsLib = () => {
  gulp.watch(['./src/scripts/lib/lib.js'], gulp.series(compileJsLib, browserReload))
}

const watchConfig = () => {
  gulp.watch('./src/config/**/*').on('change', browserSync.reload)
}

// Run esLint
const lint = () => {
  return gulp.src(['src/**/*.es6'])
    .pipe($.eslint())
    .pipe($.eslint.format())
    .pipe($.eslint.failAfterError());
}

// Start server from dev folder
const startDev = () => {
  browserSync.init({
    server: {
      baseDir: ['./dev', './src', './src/config']
    },
    open: false
  })
}

// Start server from dist folder
const startDist = () => {
  browserSync.init({
    server: {
      baseDir: ['./dist'],
    },
    port: 5000
  })
}

// Clean dev directory
const cleanDev = () => {
  return gulp.src('dev', { allowEmpty: true, read: false })
    .pipe($.clean())
}

const cleanDist = () => {
  return gulp.src('dist', { allowEmpty: true, read: false })
    .pipe($.clean())
}

// Build dist folder
const copyAssets = () => {
  return gulp.src('./src/assets/**/*')
    .pipe($.imagemin([
      $.imagemin.jpegtran({ progressive: true }),
      $.imagemin.optipng({ optimizationLevel: 5 }),
    ], { verbose: true}))
    .pipe(gulp.dest('./dist/assets'))
}

const copyConfig = () => {
  return gulp.src('./src/config/**/*')
    .pipe(gulp.dest('./dist/'))
}

const copyHtml = () => {
  return gulp.src('./dev/**/*.html')
    .pipe($.changed('./dist'))
    .pipe($.useref())
    .pipe($.if(minHtml, $.if('*.html', $.htmlmin({ collapseWhitespace: true }))))
    .pipe($.if('*.js', $.stripDebug()))
    .pipe($.if('*.css', $.cleancss({ level: 2 })))    
    .pipe(gulp.dest('./dist'))
}

const minifyMainJs = () => {
  return gulp.src('./dev/js/main.js')
    .pipe($.uglify({
      toplevel: true
    }))
    .pipe(gulp.dest('./dev/js'))
}

// Combined compile and watch tasks
const compile = gulp.parallel(compilePug, compileSass, compileJs, compileJsLib);
const watch = gulp.parallel(watchPug, watchSass, watchJs, watchJsLib, watchConfig);

// Copy assets and minify html
const copy = gulp.parallel(copyAssets, copyConfig, gulp.series(minifyMainJs, copyHtml));

// Clean both dev and dist directory
const clean = gulp.parallel(cleanDev, cleanDist);

// Serve the compile and start the server
const serve = gulp.series(compile, startDev);
const dev = gulp.parallel(serve, watch)
const build = gulp.series(cleanDist, compile, copy);
const preview = gulp.series(build, startDist);

export {
  dev,
  build,
  clean,
  preview,
  lint
}